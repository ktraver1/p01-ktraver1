//
//  ViewController.swift
//  helloWorld
//
//  Created by kevin travers on 1/28/16.
//  Copyright © 2016 Bunny Phantom. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var outputLabel: UILabel!
    @IBOutlet weak var userInput: UITextField!
    @IBOutlet weak var switchLabel: UILabel!
    @IBOutlet weak var mySwitch: UISwitch!
    @IBOutlet weak var picture: UIImageView!
    
    let arrayImages = ["hello_world_6938135540.gif","images-3.jpeg","images-2.jpeg","images.jpeg","images-2.png","images.png","512px-HelloWorld.svg.png"]
   
    
   
    @IBAction func submit(sender: AnyObject) {
        
        outputLabel.text = userInput.text
        self.view.endEditing(true)
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        picture.hidden = true
        
        mySwitch.addTarget(self, action: Selector("switchIsChanged:"), forControlEvents: UIControlEvents.ValueChanged)

        

        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func switchIsChanged(mySwitch: UISwitch) {
        if mySwitch.on {
            switchLabel.text = "ON"
            
            let random = Int(arc4random_uniform(7))
          
            var image : UIImage = UIImage(named:arrayImages[random])!
            picture.image = image
             picture.hidden = false
        } else {
            switchLabel.text = "OFF"
             picture.hidden = true
        }
    }

    override func preferredStatusBarStyle() -> UIStatusBarStyle {
        return UIStatusBarStyle.LightContent
    }
}

